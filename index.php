<?php
/**
 * Created by IntelliJ IDEA.
 * User: eleve
 * Date: 02/01/18
 * Time: 22:02
 */

session_name('ifdscms');
session_start();

ini_set('display_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/vendor/autoload.php';

require_once 'src/dbconnect.php';
require_once 'src/routing.php';
require_once 'src/utils.php';

require_once 'src/entities/Blocks.php';
require_once 'src/entities/Columns.php';
require_once 'src/entities/Contents.php';
require_once 'src/entities/Dances.php';
require_once 'src/entities/Pages.php';
require_once 'src/entities/Planning.php';
require_once 'src/entities/Prospects.php';
require_once 'src/entities/Sections.php';
require_once 'src/entities/Settings.php';
require_once 'src/entities/Topmenu.php';
require_once 'src/entities/Users.php';

require_once 'src/controllers/frontend/controller_404.php';
require_once 'src/controllers/frontend/controller_home.php';
require_once 'src/controllers/frontend/controller_custompage.php';

require_once 'src/controllers/login/controller_login.php';

require_once 'src/controllers/admin/controller_admin_home.php';
require_once 'src/controllers/admin/controller_admin_menu_edit.php';
require_once 'src/controllers/admin/controller_admin_pages_info.php';
require_once 'src/controllers/admin/controller_admin_pages_list.php';
require_once 'src/controllers/admin/controller_admin_planning.php';
require_once 'src/controllers/admin/controller_admin_planning_edit.php';
require_once 'src/controllers/admin/controller_admin_prospect_info.php';
require_once 'src/controllers/admin/controller_admin_prospects_list.php';
require_once 'src/controllers/admin/controller_admin_settings.php';
require_once 'src/controllers/admin/controller_admin_structure_edit.php';

$db = connect($config);

if (isset($_GET['disconnect']) && $_GET['disconnect'] === 'true' && !empty($_SESSION)) {
    session_destroy();
    header('Location: index.php?page=' . $GLOBALS['settings']['homePage']);
}

if ($db != NULL) {
    // Twig filesystem initialization
    $loader = new Twig_Loader_Filesystem('src/views');
    $twig = new Twig_Environment($loader, array(
        'cache' => false,
        'debug' => true,
    ));
    $twig->getExtension('Twig_Extension_Core')->setTimezone('Europe/Paris');
    $twig->addExtension(new Twig_Extension_Debug());

    // Messages system initialization
    $GLOBALS['successes'] = array();
    $GLOBALS['warnings'] = array();
    $GLOBALS['errors'] = array();

    // Adding global variables to twig
    $twig->addGlobal('session', $_SESSION);
    $twig->addGlobal('settings', $GLOBALS['settings']);
    $twig->addGlobal('url', 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/');

    $contenu = getPage($db);
    try {
        if ($contenu[1] === 0) {
            if ($contenu[0] !== "404" && $contenu[0] !== null) {
                echo $contenu[0]($db, $twig);
            } else {
                http_response_code(404);
                echo error_404($db, $twig);
                die();
            }
        } else {
            echo custompage($db, $twig);
        }
    } catch (Twig_Error_Loader $e) {
        return highlight_string("<?php\n\$data =\n" . print_r($e, true) . ";\n?>");
    } catch (Twig_Error_Runtime $e) {
        return highlight_string("<?php\n\$data =\n" . print_r($e, true) . ";\n?>");
    } catch (Twig_Error_Syntax $e) {
        return highlight_string("<?php\n\$data =\n" . print_r($e, true) . ";\n?>");
    } catch (\PHPMailer\PHPMailer\Exception $e) {
        return highlight_string("<?php\n\$data =\n" . print_r($e, true) . ";\n?>");
    }
} else {
    echo 'Le site est indisponible pour quelques instants';
}

