<?php
/**
 * Created by IntelliJ IDEA.
 * User: eleve
 * Date: 02/01/18
 * Time: 22:43
 */

// Database connection related stuff

$config['server'] = '';
$config['login'] = '';
$config['password'] = '';
$config['db'] = '';

function connect($config)
{
    try {
        $db = new PDO('mysql:host=' . $config['server'] . ';dbname=' . $config['db'] . ';charset=utf8mb4', $config['login'], $config['password']);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        $settingsQuery = $db->prepare('SELECT * FROM settings');
        $settingsQuery->execute();
        $GLOBALS['settings'] = $settingsQuery->fetch();
    } catch (Exception $e) {
        echo 'Error: ' . $e->getMessage();
        $db = NULL;
    }
    return $db;
}