<?php
/**
 * Routes to user-requested page
 *
 * @param $db
 * @return array
 */
function getPage($db)
{
    // 0 = normal user, 1 = admin
    // For pages in the admin panel, add a "admin_" prefix
    $lesPages['login'] = 'login;0';

    $lesPages['admin_home'] = 'admin_home;1';
    $lesPages['admin_menu_edit'] = 'admin_menu_edit;1';
    $lesPages['admin_pages_info'] = 'admin_pages_info;1';
    $lesPages['admin_pages_list'] = 'admin_pages_list;1';
    $lesPages['admin_planning'] = 'admin_planning;1';
    $lesPages['admin_planning_edit'] = 'admin_planning_edit;1';
    $lesPages['admin_prospect_info'] = 'admin_prospect_info;1';
    $lesPages['admin_prospects_list'] = 'admin_prospects_list;1';
    $lesPages['admin_settings'] = 'admin_settings;1';
    $lesPages['admin_structure_edit'] = 'admin_structure_edit;1';

    $type = false; // Page type, false = internal page, true = custom page
    $contenu = array(0 => null, 1 => null); // [0] is the page name, [1] determines if internal page (0) or custom page (1)

    /*
     * Getting home page from settings
     */

    $homepage = $GLOBALS['settings']['homePage'];

    /*
     * Getting custom pages list
     */

    $pages = new \ifds\entities\Pages($db);
    $pagesArray = $pages->selectAll();

    /*
     * If the 'page' GET variable value exists in the internal pages array
     */

    if (isset($_GET['page'])) {
        $page = $_GET['page']; // Set page as this GET variable
        $type = false; // Set the content as internal page
    } else {
        $page = $homepage; // Set page as homepage custompage
        $type = true; // Set the content as custompage
    }

    /*
     * If the page doesn't exist in the internal pages array
     */

    if (!isset($lesPages[$page])) {
        foreach ($pagesArray as $currentPage) { // Iterate through custom pages array
            if ($page === $currentPage['pageLinkName']) { // If the page is found in the array
                $page = 'custompage'; // Set the page as found, transfer handling to custompage controller
                $type = true; // Set the content as custompage
            }
        }
        if ($page !== 'custompage') {
            $page = "404"; // Set the page as not found, transfers to homepage
            $type = false; // Set the content as custompage
        }
    }

    $explose = null;
    $rolePage = null;
    if (isset($lesPages[$page])) { // If the page is in the internal pages array
        $explose = explode(';', $lesPages[$page]); // Explode the variable to get page name and permissions
        $rolePage = $explose[1]; // Gets page role
    } else {
        $rolePage = 0; // Sets page role as "for every user"
    }

    if (!$type) { // If content is an internal page
        if ($rolePage == 0) {
            $contenu[0] = $explose[0];
        } else {
            if (isset($_SESSION['email'])) {
                if (isset($_SESSION['role'])) {
                    if ($rolePage == $_SESSION['role']) {
                        $contenu[0] = $explose[0];
                    } else {
                        $contenu[0] = 'login';
                    }
                } else {
                    $contenu[0] = 'login';
                }
            } else {
                $contenu[0] = 'login';
            }
        }
    }

    $contenu[1] = $type ? 1 : 0;
    return $contenu;
}