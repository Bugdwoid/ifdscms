<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 1/22/18
 * Time: 10:32 PM
 */

namespace ifds\entities;


class Users
{
    private $selectUser;
    private $loginUser;

    function __construct($db)
    {
        $this->selectUser = $db->prepare('SELECT * FROM users WHERE id = :id');
        $this->loginUser = $db->prepare('SELECT * FROM users WHERE email = :email');
    }

    public function selectUser($id)
    {
        $this->selectUser->execute(array(':id' => $id));
        return $this->selectUser->fetch();
    }

    public function loginUser($email)
    {
        $this->loginUser->execute(array(':email' => $email));
        return $this->loginUser->fetch();
    }
}