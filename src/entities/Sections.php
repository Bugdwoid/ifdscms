<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 1/24/18
 * Time: 12:24 PM
 */

namespace ifds\entities;


class Sections
{
    private $selectAll;
    private $selectOne;
    private $selectPage;
    private $getPageCount;
    private $getAtPos;
    private $getNextRank;
    private $insertOne;
    private $updateOne;
    private $updatePos;
    private $getSourcePage;
    private $deleteOne;

    function __construct($db)
    {
        $this->selectAll = $db->prepare('SELECT * FROM sections');
        $this->selectOne = $db->prepare('SELECT * FROM sections WHERE sectionId = :id');
        $this->selectPage = $db->prepare('SELECT * FROM sections WHERE pageId = :id ORDER BY pageOrder ASC');
        $this->getPageCount = $db->prepare('SELECT COUNT(sectionId) as pageCount FROM sections WHERE pageId = :id');
        $this->getAtPos = $db->prepare('SELECT sectionId FROM sections WHERE pageOrder = :pageOrder AND pageId = :id');
        $this->getNextRank = $db->prepare('SELECT COUNT(sectionId)+1 FROM sections WHERE pageId = :id');
        $this->insertOne = $db->prepare('INSERT INTO sections(pageOrder, pageId)
VALUES (:pageOrder, :id)');
        $this->updateOne = $db->prepare('UPDATE sections
        SET headerContent = :headerContent, pageOrder = :pageOrder
        WHERE sectionId = :id');
        $this->updatePos = $db->prepare('UPDATE sections SET pageOrder = :pageOrder WHERE sectionId = :id');
        $this->getSourcePage = $db->prepare('SELECT pages.* FROM pages
RIGHT JOIN sections s ON pages.pageId = s.pageId
WHERE s.sectionId = :id');
        $this->deleteOne = $db->prepare('DELETE FROM sections WHERE sectionId = :id');
    }

    public function selectAll()
    {
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
    }

    public function selectOne($id)
    {
        $this->selectOne->execute(array(':id' => $id));
        return $this->selectOne->fetch();
    }

    public function selectPage($id)
    {
        $this->selectPage->execute(array(':id' => $id));
        return $this->selectPage->fetchAll();
    }

    public function getPageCount($id)
    {
        $this->getPageCount->execute(array(':id' => $id));
        return $this->getPageCount->fetch();
    }

    public function getAtPos($pageOrder, $id)
    {
        $this->getAtPos->execute(array(
            ':pageOrder' => $pageOrder,
            ':id' => $id
        ));
        return $this->getAtPos->fetch();
    }

    public function getNextRank($id)
    {
        $this->getNextRank->execute(array(':id' => $id));
        return $this->getNextRank->fetch();
    }

    public function insertOne($pageOrder, $pageId)
    {
        $this->insertOne->execute(
            array(
                ':pageOrder' => $pageOrder,
                ':id' => $pageId
            ));
        return $this->insertOne->rowCount();
    }

    public function updateOne($headerContent, $pageOrder, $sectionId)
    {
        $this->updateOne->execute(array(
            ':headerContent' => $headerContent,
            ':pageOrder' => $pageOrder,
            ':id' => $sectionId
        ));
        return $this->updateOne->rowCount();
    }

    public function updatePos($pageOrder, $sectionId)
    {
        $this->updatePos->execute(array(
            ':pageOrder' => $pageOrder,
            ':id' => $sectionId
        ));
        return $this->updatePos->rowCount();
    }

    public function getSourcePage($id)
    {
        $this->getSourcePage->execute(array(':id' => $id));
        return $this->getSourcePage->fetch();
    }

    public function deleteOne($id)
    {
        $this->deleteOne->execute(array(':id' => $id));
        return $this->deleteOne->rowCount();
    }
}