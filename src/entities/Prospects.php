<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 2/14/18
 * Time: 2:47 PM
 */

namespace ifds\entities;


class Prospects
{
    private $selectAll;
    private $selectOne;
    private $insertOne;
    private $insertPDance;
    private $getPDances;

    function __construct($db)
    {
        $this->selectAll = $db->prepare('SELECT * FROM prospects');
        $this->selectOne = $db->prepare('SELECT * FROM prospects WHERE prospectId = :id');
        $this->insertOne = $db->prepare('INSERT INTO prospects (dateInsert, lastname, firstname, birthday, address,
postalcode, city, phone1, phone2, email, website, dancer, teacher, competitor, funding, sendquotation,
sendfinancingfile, callme, callme_number, callme_datetime, message)
VALUES (:dateInsert, :lastname, :firstname, :birthday, :address, :postalcode, :city, :phone1, :phone2, :email, :website,
:dancer, :teacher, :competitor, :funding, :sendquotation, :sendfinancingfile, :callme, :callme_number, :callme_datetime,
:message)');
        $this->insertPDance = $db->prepare('INSERT INTO prospectDances (prospectId, danceAs, danceType)
VALUES (:id, :danceAs, :danceType)');
        $this->getPDances = $db->prepare('SELECT danceAs, danceType FROM prospectDances WHERE prospectId = :id');
    }

    public function selectAll()
    {
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
    }

    public function selectOne($id)
    {
        $this->selectOne->execute(array(':id' => $id));
        return $this->selectOne->fetch();
    }

    public function insertOne($dateInsert, $lastname, $firstname, $birthday, $address, $postalcode, $city, $phone1,
                              $phone2, $email, $website, $dancer, $teacher, $competitor, $funding, $sendquotation,
                              $sendfinancingfile, $callme, $callmeNumber, $callmeDatetime, $message)
    {
        $this->insertOne->execute(array(
            ':dateInsert' => $dateInsert,
            ':lastname' => $lastname,
            ':firstname' => $firstname,
            ':birthday' => $birthday,
            ':address' => $address,
            ':postalcode' => $postalcode,
            ':city' => $city,
            ':phone1' => $phone1,
            ':phone2' => $phone2,
            ':email' => $email,
            ':website' => $website,
            ':dancer' => $dancer,
            ':teacher' => $teacher,
            ':competitor' => $competitor,
            ':funding' => $funding,
            ':sendquotation' => $sendquotation,
            ':sendfinancingfile' => $sendfinancingfile,
            ':callme' => $callme,
            ':callme_number' => $callmeNumber,
            ':callme_datetime' => $callmeDatetime,
            ':message' => $message
        ));
        return $this->insertOne->rowCount();
    }

    public function insertPDance($id, $danceAs, $danceType)
    {
        $this->insertPDance->execute(array(
            ':id' => $id,
            ':danceAs' => $danceAs,
            ':danceType' => $danceType));
        return $this->insertPDance->rowCount();
    }

    public function getPDances($id)
    {
        $this->getPDances->execute(array(':id' => $id));
        return $this->getPDances->fetchAll();
    }
}