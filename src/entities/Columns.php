<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 1/24/18
 * Time: 12:24 PM
 */

namespace ifds\entities;


class Columns
{
    private $selectAll;
    private $selectOne;
    private $selectFromPage;
    private $getSectionCount;
    private $getAtPos;
    private $selectWithSection;
    private $getNextRank;
    private $insertOne;
    private $updateOne;
    private $updatePos;
    private $getSourcePage;
    private $deleteOne;

    function __construct($db)
    {
        $this->selectAll = $db->prepare('SELECT * FROM columns');
        $this->selectOne = $db->prepare('SELECT * FROM columns WHERE columnId = :id');
        $this->selectFromPage = $db->prepare('SELECT * FROM columns
INNER JOIN sections s ON columns.sectionId = s.sectionId
WHERE s.pageId = :id
ORDER BY sectionOrder ASC');
        $this->getSectionCount = $db->prepare('SELECT COUNT(columnId) as sectionCount FROM columns WHERE sectionId = :id');
        $this->getAtPos = $db->prepare('SELECT columnId FROM columns WHERE sectionOrder = :sectionOrder AND sectionId = :id');
        $this->selectWithSection = $db->prepare('SELECT * FROM columns WHERE sectionId = :id');
        $this->getNextRank = $db->prepare('SELECT COUNT(columnId)+1 FROM columns WHERE sectionId = :id');
        $this->insertOne = $db->prepare('INSERT INTO columns(columnWidth, sectionOrder, sectionId)
VALUES (:columnWidth, :sectionOrder, :id)');
        $this->updateOne = $db->prepare('UPDATE columns SET columnWidth = :columnWidth, sectionOrder = :sectionOrder
WHERE columnId = :id');
        $this->updatePos = $db->prepare('UPDATE columns SET sectionOrder = :sectionOrder WHERE columnId = :id');
        $this->getSourcePage = $db->prepare('SELECT pages.* FROM pages
RIGHT JOIN sections s ON pages.pageId = s.pageId
RIGHT JOIN columns c ON s.sectionId = c.sectionId
WHERE c.columnId = :id');
        $this->deleteOne = $db->prepare('DELETE FROM columns WHERE columnId = :id');
    }

    public function selectAll()
    {
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
    }

    public function selectOne($id)
    {
        $this->selectOne->execute(array(':id' => $id));
        return $this->selectOne->fetch();
    }

    public function selectFromPage($id)
    {
        $this->selectFromPage->execute(array(':id' => $id));
        return $this->selectFromPage->fetchAll();
    }

    public function getSectionCount($id)
    {
        $this->getSectionCount->execute(array(':id' => $id));
        return $this->getSectionCount->fetch();
    }

    public function getAtPos($sectionOrder, $id)
    {
        $this->getAtPos->execute(array(
            ':sectionOrder' => $sectionOrder,
            ':id' => $id
        ));
        return $this->getAtPos->fetch();
    }

    public function selectWithSection($id)
    {
        $this->selectWithSection->execute(array(':id' => $id));
        return $this->selectWithSection->fetchAll();
    }

    public function getNextRank($id)
    {
        $this->getNextRank->execute(array(':id' => $id));
        return $this->getNextRank->fetch();
    }

    public function insertOne($columnWidth, $sectionOrder, $sectionId)
    {
        $this->insertOne->execute(array(
            ':columnWidth' => $columnWidth,
            ':sectionOrder' => $sectionOrder,
            ':id' => $sectionId
        ));
        return $this->insertOne->fetchAll();
    }

    public function updateOne($columnWidth, $sectionOrder, $columnId)
    {
        $this->updateOne->execute(array(
            ':columnWidth' => $columnWidth,
            ':sectionOrder' => $sectionOrder,
            ':id' => $columnId
        ));
        return $this->updateOne->rowCount();
    }

    public function updatePos($sectionOrder, $sectionId)
    {
        $this->updatePos->execute(array(
            ':sectionOrder' => $sectionOrder,
            ':id' => $sectionId
        ));
        return $this->updatePos->rowCount();
    }

    public function getSourcePage($id)
    {
        $this->getSourcePage->execute(array(':id' => $id));
        return $this->getSourcePage->fetch();
    }

    public function deleteOne($id)
    {
        $this->deleteOne->execute(array(':id' => $id));
        return $this->deleteOne->rowCount();
    }
}