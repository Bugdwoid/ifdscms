<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 1/24/18
 * Time: 12:24 PM
 */

namespace ifds\entities;


class Blocks
{
    private $selectAll;
    private $selectOne;
    private $selectAllFromPage;
    private $getNextRank;
    private $getBlockContent;
    private $getSourcePage;
    private $insertOne;
    private $updateOne;
    private $updateContentType;
    private $deleteOne;

    function __construct($db)
    {
        $this->selectAll = $db->prepare('SELECT * FROM blocks');
        $this->selectOne = $db->prepare('SELECT * FROM blocks WHERE blockId = :id');
        $this->selectAllFromPage = $db->prepare('SELECT b.blockId, b.blockStyle, b.blockBackground, b.columnOrder, b.columnId, b.contentType,
  i.imageTitle, i.imageSource, i.paramFill, i.imgSize as ImageSize,
  ti.textimageTitle, ti.textimageImgSource, ti.textimageTextContent, ti.textimageImgPos, ti.imgSize as tiImageSize,
  h.htmlTitle, h.htmlContent
FROM blocks b
  INNER JOIN columns c
    ON b.columnId = c.columnId
  INNER JOIN sections s
    ON c.sectionId = s.sectionId
  LEFT JOIN contents_images i
    ON b.blockId = i.blockId
  LEFT JOIN contents_textimage ti
    ON b.blockId = ti.blockId
  LEFT JOIN contents_html h
    ON b.blockId = h.blockId
WHERE s.pageId = :id
ORDER BY columnOrder ASC;');
        $this->getNextRank = $db->prepare('SELECT COUNT(blockId)+1 FROM blocks WHERE columnId = :id');
        $this->getBlockContent = $db->prepare('SELECT b.blockId, b.blockStyle, b.blockBackground, b.columnOrder, b.columnId, b.contentType,
  i.imageTitle, i.imageSource, i.paramFill, i.imgSize as ImageSize,
  ti.textimageTitle, ti.textimageImgSource, ti.textimageTextContent, ti.textimageImgPos, ti.imgSize as tiImageSize,
  h.htmlTitle, h.htmlContent
FROM blocks b
  LEFT OUTER JOIN contents_images i ON b.blockId = i.blockId
  LEFT OUTER JOIN contents_textimage ti ON b.blockId = ti.blockId
  LEFT OUTER JOIN contents_html h ON b.blockId = h.blockId
WHERE b.blockId = :id;');
        $this->getSourcePage = $db->prepare('SELECT pages.* FROM pages
RIGHT JOIN sections s ON pages.pageId = s.pageId
RIGHT JOIN columns c ON s.sectionId = c.sectionId
RIGHT JOIN blocks b ON c.columnId = b.columnId
WHERE b.blockId = :id;');
        $this->insertOne = $db->prepare('INSERT INTO blocks(columnOrder, columnId)
VALUES (:columnOrder, :id)');
        $this->updateOne = $db->prepare('UPDATE blocks SET columnOrder = :columnOrder WHERE blockId = :id');
        $this->updateContentType = $db->prepare('UPDATE blocks SET contentType = :type WHERE blockId = :id;');
        $this->deleteOne = $db->prepare('DELETE FROM blocks WHERE blockId = :id');
    }

    public function selectAll()
    {
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
    }

    public function selectOne($id)
    {
        $this->selectOne->execute(array(':id' => $id));
        return $this->selectOne->fetch();
    }

    public function selectAllFromPage($id)
    {
        $this->selectAllFromPage->execute(array(':id' => $id));
        return $this->selectAllFromPage->fetchAll();
    }

    public function getNextRank($id)
    {
        $this->getNextRank->execute(array(':id' => $id));
        return $this->getNextRank->fetch();
    }
    
    public function getBlockContent($id)
    {
        $this->getBlockContent->execute(array(':id' => $id));
        return $this->getBlockContent->fetch();
    }

    public function getSourcePage($id)
    {
        $this->getSourcePage->execute(array(':id' => $id));
        return $this->getSourcePage->fetch();
    }

    public function insertOne($columnOrder, $columnId)
    {
        $this->insertOne->execute(array(
            ':columnOrder' => $columnOrder,
            ':id' => $columnId
        ));
        return $this->insertOne->rowCount();
    }

    public function updateOne($columnOrder, $blockId)
    {
        $this->updateOne->execute(array(
            ':columnOrder' => $columnOrder,
            ':id' => $blockId
        ));
        return $this->updateOne->rowCount();
    }

    public function updateContentType($contentType, $blockId)
    {
        $this->updateContentType->execute(array(
            ':type' => $contentType,
            ':id' => $blockId
        ));
        return $this->updateContentType->rowCount();
    }

    public function deleteOne($id)
    {
        $this->deleteOne->execute(array(':id' => $id));
        return $this->deleteOne->rowCount();
    }
}