<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 2/14/18
 * Time: 3:31 PM
 */

namespace ifds\entities;


class Dances
{
    private $selectAll;
    private $selectOne;
    private $insertOne;

    function __construct($db)
    {
        $this->selectAll = $db->prepare('SELECT * FROM danceTypes');
        $this->selectOne = $db->prepare('SELECT * FROM danceTypes WHERE danceId = :id');
        $this->insertOne = $db->prepare('INSERT INTO danceTypes (danceName) VALUES (:danceName)');
    }

    public function selectAll()
    {
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
    }

    public function selectOne($id)
    {
        $this->selectOne->execute(array(':id' => $id));
        return $this->selectOne->fetch();
    }

    public function insertOne($danceName)
    {
        $this->insertOne->execute(array(':danceName' => $danceName));
        return $this->insertOne->rowCount();
    }
}