<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 2/18/18
 * Time: 2:46 PM
 */

namespace ifds\entities;


class Planning
{
    private $selectAll;
    private $selectOne;
    private $insertOne;
    private $updateOne;
    private $deleteOne;

    function __construct($db)
    {
        $this->selectAll = $db->prepare('SELECT
  pDateId,
  startDate,
  endDate,
  link,
  content,
  datediff(endDate, startDate) as dateDiff
FROM planning');
        $this->selectOne = $db->prepare('SELECT
  pDateId,
  startDate,
  endDate,
  link,
  content,
  datediff(endDate, startDate) as dateDiff
FROM planning WHERE pDateId = :id');
        $this->insertOne = $db->prepare('INSERT INTO planning (startDate, endDate, link, content)
VALUES (:startDate, :endDate, :link, :content)');
        $this->updateOne = $db->prepare('UPDATE planning SET startDate = :startDate, endDate = :endDate, link = :link,
content = :content WHERE pDateId = :id');
        $this->deleteOne = $db->prepare('DELETE FROM planning WHERE pDateId = :id');
    }

    public function selectAll()
    {
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
    }

    public function selectOne($id)
    {
        $this->selectOne->execute(array(':id' => $id));
        return $this->selectOne->fetch();
    }

    public function insertOne($startDate, $endDate, $link, $content)
    {
        $this->insertOne->execute(array(
            ':startDate' => $startDate,
            ':endDate' => $endDate,
            ':link' => $link,
            ':content' => $content
        ));
        return $this->insertOne->rowCount();
    }

    public function updateOne($startDate, $endDate, $link, $content, $id)
    {
        $this->updateOne->execute(array(
            ':startDate' => $startDate,
            ':endDate' => $endDate,
            ':link' => $link,
            ':content' => $content,
            ':id' => $id
        ));
        return $this->updateOne->rowCount();
    }

    public function deleteOne($id)
    {
        $this->deleteOne->execute(array(

        ));
        return $this->deleteOne->rowCount();
    }
}