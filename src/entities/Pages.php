<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 1/23/18
 * Time: 4:26 PM
 */

namespace ifds\entities;


class Pages
{
    private $selectAll;
    private $getAllNames;
    private $selectOneLinkName;
    private $insertOne;
    private $updateOne;
    private $deleteOne;

    function __construct($db)
    {
        $this->selectAll = $db->prepare('SELECT * FROM pages');
        $this->getAllNames = $db->prepare('SELECT name FROM pages');
        $this->selectOneLinkName = $db->prepare('SELECT * FROM pages WHERE pageLinkName = :pageLinkName');
        $this->insertOne = $db->prepare('INSERT INTO pages (pages.name, pages.pageLinkName)
VALUES (:name, :pageLinkName);');
        $this->updateOne = $db->prepare('UPDATE pages SET name = :name, pageLinkName = :pageLinkName,
paramHugeHeader = :paramHugeHeader, headerTitleText = :headerTitleText, headerCaptionText = :headerCaptionText,
headerButtonText = :headerButtonText WHERE pageId = :id');
        $this->deleteOne = $db->prepare('DELETE FROM pages WHERE pageId = :id');
    }

    public function selectAll()
    {
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
    }

    public function getAllNames()
    {
        $this->getAllNames->execute();
        return $this->getAllNames->fetchAll();
    }

    public function selectOneLinkName($pageLinkName)
    {
        $this->selectOneLinkName->execute(array(':pageLinkName' => $pageLinkName));
        return $this->selectOneLinkName->fetch();
    }

    public function insertOne($name, $pageLinkName)
    {
        $this->insertOne->execute(
            array(
                ':name' => $name,
                ':pageLinkName' => $pageLinkName
            ));
        return $this->insertOne->rowCount();
    }

    public function updateOne($name, $pageLinkName, $paramHugeHeader, $headerTitleText, $headerCaptionText, $headerButtonText, $id)
    {
        $this->updateOne->execute(
            array(
                ':name' => $name,
                ':pageLinkName' => $pageLinkName,
                ':paramHugeHeader' => $paramHugeHeader,
                ':headerTitleText' => $headerTitleText,
                ':headerCaptionText' => $headerCaptionText,
                ':headerButtonText' => $headerButtonText,
                ':id' => $id
            ));
        return $this->updateOne->rowCount();
    }

    public function deleteOne($id)
    {
        $this->deleteOne->execute(array(':id' => $id));
        return $this->deleteOne->rowCount();
    }
}