<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 2/23/18
 * Time: 5:43 PM
 */

namespace entities;


class Topmenu
{
    private $selectAll;
    private $selectPrimary;
    private $selectChildren;
    private $insertOne;
    private $updateOne;
    private $updatePos;
    private $deleteOne;

    function __construct($db)
    {
        $this->selectAll = $db->prepare('SELECT * FROM topmenu');
        $this->selectPrimary = $db->prepare('SELECT * FROM topmenu WHERE isPrimary = \'1\' ORDER BY menuOrder');
        $this->selectChildren = $db->prepare('SELECT * FROM topmenu WHERE isPrimary = \'0\' ORDER BY parent, menuOrder');
        $this->insertOne = $db->prepare('INSERT INTO topmenu (menuOrder, isPrimary, parent, pageId)
VALUES (:menuOrder, :isPrimary, :parent, :pageId)');
        $this->updateOne = $db->prepare('UPDATE topmenu SET menuOrder = :menuOrder, isPrimary = :isPrimary,
parent = :parent, pageId = :pageId WHERE linkId = :id');
        $this->updatePos = $db->prepare('UPDATE topmenu SET menuOrder = :menuOrder WHERE linkId = :id');
        $this->deleteOne = $db->prepare('DELETE FROM topmenu WHERE linkId = :id');
    }

    public function selectAll()
    {
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
    }

    public function selectPrimary()
    {
        $this->selectPrimary->execute();
        return $this->selectPrimary->fetchAll();
    }

    public function selectChildren()
    {
        $this->selectChildren->execute();
        return $this->selectChildren->fetchAll();
    }

    public function insertOne($menuOrder, $isPrimary, $parent, $pageId)
    {
        $this->insertOne->execute(array(
            ':menuOrder' => $menuOrder,
            ':isPrimary' => $isPrimary,
            ':parent' => $parent,
            ':pageId' => $pageId
        ));
        return $this->insertOne->rowCount();
    }

    public function updateOne($menuOrder, $isPrimary, $parent, $pageId, $id)
    {
        $this->updateOne->execute(array(
            ':menuOrder' => $menuOrder,
            ':isPrimary' => $isPrimary,
            ':parent' => $parent,
            ':pageId' => $pageId,
            ':id' => $id
        ));
        return $this->updateOne->rowCount();
    }

    public function updatePos($menuOrder, $id)
    {
        $this->updatePos->execute(array(
            ':menuOrder' => $menuOrder,
            ':id' => $id
        ));
        return $this->updatePos->rowCount();
    }

    public function deleteOne($id)
    {
        $this->deleteOne->execute(array(
            ':id' => $id
        ));
        return $this->deleteOne->rowCount();
    }
}