<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 1/24/18
 * Time: 12:24 PM
 */

namespace ifds\entities;


class Contents
{
    private $selectTypes;
    private $selectImgPos;
    private $selectImgSize;
    private $updateImage;
    private $updateTextimage;
    private $updateHtml;
    private $insertImage;
    private $insertTextimage;
    private $insertHtml;

    function __construct($db)
    {
        $this->selectTypes = $db->prepare('SELECT typeId, name FROM contentTypes');
        $this->selectImgPos = $db->prepare('SELECT posId, name FROM textimage_imgposition');
        $this->selectImgSize = $db->prepare('SELECT sizeId, name FROM imgSize');
        $this->updateImage = $db->prepare('UPDATE contents_images
        SET imageTitle = :title, imageSource = :source, paramFill = :paramFill, imgSize = :imgSize
        WHERE blockId = :id');
        $this->updateTextimage = $db->prepare('UPDATE contents_textimage
        SET textimageTitle = :title, textimageImgSource = :source, textimageTextContent = :content, textimageImgPos = :pos, imgSize = :imgSize
        WHERE blockId = :id');
        $this->updateHtml = $db->prepare('UPDATE contents_html
        SET htmlTitle = :title, htmlContent = :content
        WHERE blockId = :id');
        $this->insertImage = $db->prepare('INSERT INTO contents_images (imageTitle, imageSource, paramFill, imgSize, blockId) VALUES (:imageTitle, :imageSource, :paramFill, :imgSize, :blockId);');
        $this->insertTextimage = $db->prepare('INSERT INTO contents_textimage (textimageTitle, textimageImgSource, textimageTextContent, textimageImgPos, imgSize, blockId)
VALUES (:textimageTitle, :textimageImgSource, :textimageTextContent, :textimageImgPos, :imgSize, :blockId);');
        $this->insertHtml = $db->prepare('INSERT INTO contents_html (htmlTitle, htmlContent, blockId) VALUES (:htmlTitle, :htmlContent, :blockId);');
    }

    public function selectTypes()
    {
        $this->selectTypes->execute();
        return $this->selectTypes->fetchAll();
    }

    public function selectImgPos()
    {
        $this->selectImgPos->execute();
        return $this->selectImgPos->fetchAll();
    }

    public function selectImgSize()
    {
        $this->selectImgSize->execute();
        return $this->selectImgSize->fetchAll();
    }

    public function updateImage($id, $title, $paramFill, $imgSize, $source)
    {
        $this->updateImage->execute(array(
            ':title' => $title,
            ':source' => $source,
            ':paramFill' => $paramFill,
            ':imgSize' => $imgSize,
            ':id' => $id
        ));
        return $this->updateImage->rowCount();
    }

    public function updateTextimage($id, $title, $imgSource, $textContent, $imgSize, $imgPos)
    {
        $this->updateTextimage->execute(array(
            ':title' => $title,
            ':source' => $imgSource,
            ':content' => $textContent,
            ':pos' => $imgPos,
            ':imgSize' => $imgSize,
            ':id' => $id
        ));
        return $this->updateTextimage->rowCount();
    }

    public function updateHtml($id, $title, $content)
    {
        $this->updateHtml->execute(array(
            ':title' => $title,
            ':content' => $content,
            ':id' => $id
        ));
        return $this->updateHtml->rowCount();
    }

    public function insertImage($imageTitle, $imageSource, $paramFill, $imgSize, $blockId)
    {
        $this->insertImage->execute(array(
            ':imageTitle' => $imageTitle,
            ':imageSource' => $imageSource,
            ':paramFill' => $paramFill,
            ':imgSize' => $imgSize,
            ':blockId' => $blockId
        ));
        return $this->insertImage->rowCount();
    }

    public function insertTextimage($textimageTitle, $textimageImgSource, $textimageTextContent, $textimageImgPos, $imgSize, $blockId)
    {
        $this->insertTextimage->execute(array(
            ':textimageTitle' => $textimageTitle,
            ':textimageImgSource' => $textimageImgSource,
            ':textimageTextContent' => $textimageTextContent,
            ':textimageImgPos' => $textimageImgPos,
            ':imgSize' => $imgSize,
            ':blockId' => $blockId
        ));
        return $this->insertTextimage->rowCount();
    }

    public function insertHtml($htmlTitle, $htmlContent, $blockId)
    {
        $this->insertHtml->execute(array(
            ':htmlTitle' => $htmlTitle,
            ':htmlContent' => $htmlContent,
            ':blockId' => $blockId
        ));
        return $this->insertHtml->rowCount();
    }
}