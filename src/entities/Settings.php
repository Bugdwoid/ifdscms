<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 2/20/18
 * Time: 5:42 PM
 */

namespace ifds\entities;


class Settings
{
    private $select;
    private $update;

    function __construct($db)
    {
        $this->select = $db->prepare('SELECT * FROM settings');
        $this->update = $db->prepare('UPDATE settings SET homePage = :homePage, mailSender = :mailSender,
mailSenderAddress = :mailSenderAddress, mailRecipient = :mailRecipient, mailRecipientAddress = :mailRecipientAddress');
    }

    public function select()
    {
        $this->select->execute();
        return $this->select->fetch();
    }

    public function update($homePage, $mailSender, $mailSenderAddress, $mailRecipient, $mailRecipientAddress)
    {
        $this->update->execute(array(
            ':homePage' => $homePage,
            ':mailSender' => $mailSender,
            ':mailSenderAddress' => $mailSenderAddress,
            ':mailRecipient' => $mailRecipient,
            ':mailRecipientAddress' => $mailRecipientAddress
        ));
        return $this->update->rowCount();
    }
}