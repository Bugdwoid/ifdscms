<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 2/20/18
 * Time: 6:03 PM
 */

function admin_planning($db, $twig)
{
    $planning = new \ifds\entities\Planning($db);
    if (isset($_POST['addEventBt'])) {
        if ($planning->insertOne(date("Y/m/d", strtotime($_POST['startDate'])), date("Y/m/d", strtotime($_POST['endDate'])), $_POST['link'], $_POST['content']) === 1) {
            $GLOBALS['successes'][] = 'L\'entrée a été ajoutée.';
        } else {
            $GLOBALS['errors'][] = 'Erreur à l\'ajout de l\'entrée.';
        }
    }

    if (isset($_GET['deleteEvent'])) {
        if ($planning->deleteOne($_GET['deleteEvent'])) {
            $GLOBALS['successes'][] = 'L\'entrée a été supprimée.';
        } else {
            $GLOBALS['errors'][] = 'Erreur à la modification de l\'entrée.';
        }
    }

    $fullPlanning = $planning->selectAll();
    return $twig->render('admin/planning.html.twig', array('planning' => $fullPlanning,
        'successes' => $GLOBALS['successes'], 'warnings' => $GLOBALS['warnings'], 'errors' => $GLOBALS['errors']));
}