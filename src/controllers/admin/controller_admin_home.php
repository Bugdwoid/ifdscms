<?php
/**
 * Created by IntelliJ IDEA.
 * User: eleve
 * Date: 05/01/18
 * Time: 22:55
 */

function admin_home($db, $twig)
{
    $pages = new \ifds\entities\Pages($db);
    $pagesArray = $pages->selectAll();
    return $twig->render('admin/index.html.twig', array('pages' => $pagesArray,
        'successes' => $GLOBALS['successes'], 'warnings' => $GLOBALS['warnings'], 'errors' => $GLOBALS['errors']));
}