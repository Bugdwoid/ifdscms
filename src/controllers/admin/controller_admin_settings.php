<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 2/20/18
 * Time: 5:24 PM
 */

function admin_settings($db, $twig)
{
    $pages = new \ifds\entities\Pages($db);
    $settings = new \ifds\entities\Settings($db);

    if (isset($_POST['settingsBt'])) {
        if ($settings->update($_POST['homePage'], $_POST['mailSender'], $_POST['mailSenderAddress'],
                $_POST['mailRecipient'], $_POST['mailRecipientAddress']) === 1) {
            $GLOBALS['successes'][] = 'Les paramètres ont été modifiés.';
        } else {
            if ((string)$GLOBALS['settings']['homePage'] === (string)$_POST['homePage']
                && (string)$GLOBALS['settings']['mailSender'] === (string)$_POST['mailSender']
                && (string)$GLOBALS['settings']['mailSenderAddress'] === (string)$_POST['mailSenderAddress']
                && (string)$GLOBALS['settings']['mailRecipient'] === (string)$_POST['mailRecipient']
                && (string)$GLOBALS['settings']['mailRecipientAddress'] === (string)$_POST['mailRecipientAddress']) {
                $GLOBALS['warnings'][] = 'Les informations entrées sont identiques aux informations existantes.';
            } else {
                $GLOBALS['errors'][] = 'Erreur à la modification des paramètres.';
            }
        }
    }

    $GLOBALS['settings'] = $settings->select();
    $pagesList = $pages->selectAll();
    return $twig->render('admin/settings.html.twig', array('pagesList' => $pagesList, 'settings' => $GLOBALS['settings'],
        'successes' => $GLOBALS['successes'], 'warnings' => $GLOBALS['warnings'], 'errors' => $GLOBALS['errors']));
}