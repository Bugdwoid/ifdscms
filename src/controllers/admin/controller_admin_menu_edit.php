<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 2/26/18
 * Time: 3:36 PM
 */

function admin_menu_edit($db, $twig)
{
    $pages = new \ifds\entities\Pages($db);
    $topmenu = new \entities\Topmenu($db);

    $pagesArray = $pages->selectAll();
    $topmenuPrimary = $topmenu->selectPrimary();
    $topmenuChildren = $topmenu->selectChildren();

    return $twig->render('admin/menuEdit.html.twig', array('pages' => $pagesArray,
        'primaryTopmenu' => $topmenuPrimary, 'secondaryTopmenu' => $topmenuChildren,
        'successes' => $GLOBALS['successes'], 'warnings' => $GLOBALS['warnings'], 'errors' => $GLOBALS['errors']));
}