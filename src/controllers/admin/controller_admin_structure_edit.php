<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 1/31/18
 * Time: 5:21 PM
 */

function admin_structure_edit($db, $twig)
{
    $section = null;
    $column = null;
    $block = null;
    $type = null;
    $sourcePage = null;
    $contentTypes = null;
    $imgPositions = null;
    $imgSizes = null;
    $pageCount = null;
    $sectionCount = null;

    if (isset($_GET['type'])) {
        $type = $_GET['type'];

        if (isset($_GET['sectionId']) && $_GET['type'] === 'section') {
            $sections = new \ifds\entities\Sections($db);

            $sourcePage = $sections->getSourcePage($_GET['sectionId']);
            $section = $sections->selectOne($_GET['sectionId']);

            if (isset($_POST['editStructureBt'])) {
                $targetPosElement = $sections->getAtPos($_POST['position'], $sourcePage['pageId']);
                $currentPosSection = $section['pageOrder'];
                if ($sections->updateOne($_POST['headerContent'], $_POST['position'], $_GET['sectionId']) === 1) {
                    if ($targetPosElement['sectionId'] !== $section['sectionId']) {
                        $sections->updatePos($currentPosSection, $targetPosElement['sectionId']);
                        $GLOBALS['successes'][] = 'Positions interverties !';
                    }
                    $GLOBALS['successes'][] = 'Modification de la section réussie !';
                } else {
                    if ((string)$section['sectionId'] === (string)$_GET['sectionId']
                        && (string)$section['pageOrder'] === (string)$_POST['position']
                        && (string)$section['headerContent'] === (string)$_POST['headerContent']) {
                        $GLOBALS['warnings'][] = 'Les informations entrées sont identiques aux informations existantes.';
                    } else {
                        $GLOBALS['errors'][] = 'Erreur à la modification de la section.';
                    }
                }
            }


            $section = $sections->selectOne($_GET['sectionId']);
            $pageCount = $sections->getPageCount($sourcePage['pageId']);
        }

        if (isset($_GET['columnId']) && $_GET['type'] === 'column') {
            $columns = new \ifds\entities\Columns($db);

            $sourcePage = $columns->getSourcePage($_GET['columnId']);
            $column = $columns->selectOne($_GET['columnId']);

            if (isset($_POST['editStructureBt'])) {
                $targetPosElement = $columns->getAtPos($_POST['position'], $column['sectionId']);
                $currentPosSection = $column['sectionOrder'];

                if ($columns->updateOne($_POST['width'], $_POST['position'], $_GET['columnId']) === 1) {
                    if ($targetPosElement['columnId'] !== $column['columnId']) {
                        $columns->updatePos($currentPosSection, $targetPosElement['columnId']);
                        $GLOBALS['successes'][] = 'Positions interverties !';
                    }
                    $GLOBALS['successes'][] = 'Modification de la colonne réussie !';
                } else {
                    if ((string)$section['columnId'] === (string)$_GET['columnId']
                        && (string)$section['columnWidth'] === (string)$_POST['width']
                        && (string)$section['sectionOrder'] === (string)$_POST['position']) {
                        $GLOBALS['warnings'][] = 'Les informations entrées sont identiques aux informations existantes.';
                    } else {
                        $GLOBALS['errors'][] = 'Erreur à la modification de la colonne.';
                    }
                }
            }

            $column = $columns->selectOne($_GET['columnId']);
            $sectionCount = $columns->getSectionCount($column['sectionId']);
        }

        if (isset($_GET['blockId']) && $_GET['type'] === 'block') {
            $blocks = new \ifds\entities\Blocks($db);
            $contents = new \ifds\entities\Contents($db);

            if (isset($_POST['editStructureBt'])) {
                if ($blocks->updateOne($_POST['position'], $_GET['blockId']) === 1) {
                    $GLOBALS['successes'][] = 'Modification du bloc réussie !';
                } else {
                    $GLOBALS['errors'][] = 'Erreur à la modification du bloc.';
                }
            }
            if (isset($_POST['editImageBlockBt'])) {
                if ($contents->updateImage($_GET['blockId'], $_POST['title'], $_POST['paramFill'], $_POST['imgSize'], $_POST['source']) === 1) {
                    $GLOBALS['successes'][] = 'Modification de l\'image réussie !';
                } else {
                    $GLOBALS['errors'][] = 'Erreur à la modification de l\'image.';
                }
            }
            if (isset($_POST['editTextimageBlockBt'])) {
                if ($contents->updateTextimage($_GET['blockId'], $_POST['title'], $_POST['source'], $_POST['textContent'], $_POST['imgSize'], $_POST['pos']) === 1) {
                    $GLOBALS['successes'][] = 'Modification du bloc de texte et image réussie !';
                } else {
                    $GLOBALS['errors'][] = 'Erreur à la modification du bloc de texte et image.';
                }
            }
            if (isset($_POST['editHtmlBlockBt'])) {
                if ($contents->updateHtml($_GET['blockId'], $_POST['title'], $_POST['content']) === 1) {
                    $GLOBALS['successes'][] = 'Modification du bloc HTML réussie !';
                } else {
                    $GLOBALS['errors'][] = 'Erreur à la modification du bloc HTML.';
                }
            }

            if (isset($_POST['addContentBlockBt'])) {
                switch ($_POST['contentType']) {
                    case 2:
                        if ($contents->insertImage(null, null, 0, 4, $_GET['blockId']) === 1
                            && $blocks->updateContentType(2, $_GET['blockId']) === 1) {
                            $GLOBALS['successes'][] = 'Modification du bloc HTML réussie !';
                        } else {
                            $GLOBALS['errors'][] = 'Erreur à la modification du bloc HTML.';
                        }
                        break;
                    case 3:
                        if ($contents->insertTextimage(null, null, null, 1, 4, $_GET['blockId']) === 1
                            && $blocks->updateContentType(3, $_GET['blockId']) === 1) {
                            $GLOBALS['successes'][] = 'Modification du bloc HTML réussie !';
                        } else {
                            $GLOBALS['errors'][] = 'Erreur à la modification du bloc HTML.';
                        }
                        break;
                    case 4:
                        if ($contents->insertHtml(null, null, $_GET['blockId']) === 1
                            && $blocks->updateContentType(4, $_GET['blockId']) === 1) {
                            $GLOBALS['successes'][] = 'Modification du bloc HTML réussie !';
                        } else {
                            $GLOBALS['errors'][] = 'Erreur à la modification du bloc HTML.';
                        }
                        break;
                    case 5:
                        if ($blocks->updateContentType(5, $_GET['blockId']) === 1) {
                            $GLOBALS['successes'][] = 'Modification du bloc formulaire réussie !';
                        } else {
                            $GLOBALS['errors'][] = 'Erreur à la modification du bloc formulaire.';
                        }
                        break;
                    case 6:
                        if ($blocks->updateContentType(6, $_GET['blockId']) === 1) {
                            $GLOBALS['successes'][] = 'Modification du bloc planning réussie !';
                        } else {
                            $GLOBALS['errors'][] = 'Erreur à la modification du bloc planning.';
                        }
                        break;
                }
            }

            $block = $blocks->getBlockContent($_GET['blockId']);
            $sourcePage = $blocks->getSourcePage($_GET['blockId']);
            $contentTypes = $contents->selectTypes();
            $imgPositions = $contents->selectImgPos();
            $imgSizes = $contents->selectImgSize();
        }
    }
    return $twig->render('admin/structureEdit.html.twig', array('type' => $type, 'section' => $section, 'column' => $column,
        'block' => $block, 'contentTypes' => $contentTypes, 'imgPos' => $imgPositions, 'imgSizes' => $imgSizes,
        'sourcePage' => $sourcePage, 'pageCount' => $pageCount, 'sectionCount' => $sectionCount,
        'successes' => $GLOBALS['successes'], 'warnings' => $GLOBALS['warnings'], 'errors' => $GLOBALS['errors']));

}