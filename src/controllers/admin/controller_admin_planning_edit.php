<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 2/22/18
 * Time: 5:48 PM
 */

function admin_planning_edit($db, $twig)
{
    $planning = new \ifds\entities\Planning($db);
    $event = array();

    if (isset($_GET['eventId'])) {
        $event = $planning->selectOne($_GET['eventId']);
        if (isset($_POST['editEventBt'])) {
            $startDate = strtotime($_POST['startDate']);
            $endDate = strtotime($_POST['endDate']);

            if ($planning->updateOne(date("Y/m/d", $startDate), date("Y/m/d", $endDate), $_POST['link'], $_POST['content'], $_GET['eventId']) === 1) {
                $GLOBALS['successes'][] = 'L\'entrée a été modifiée.';
            } else {
                $GLOBALS['errors'][] = 'Erreur à la modification de l\'entrée.';
            }
        }
        $event = $planning->selectOne($_GET['eventId']);
    }

    return $twig->render('admin/planningEdit.html.twig', array('event' => $event,
        'successes' => $GLOBALS['successes'], 'warnings' => $GLOBALS['warnings'], 'errors' => $GLOBALS['errors']));
}