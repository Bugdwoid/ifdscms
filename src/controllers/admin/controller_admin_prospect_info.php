<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 2/20/18
 * Time: 2:17 PM
 */

function admin_prospect_info($db, $twig)
{
    $prospects = new \ifds\entities\Prospects($db);
    $danceTypes = new \ifds\entities\Dances($db);
    $dances = null;
    $prospect = null;
    $prospectDances = null;

    if (isset($_GET['prospect'])) {
        $dances = $danceTypes->selectAll();
        $prospect = $prospects->selectOne($_GET['prospect']);
        $prospectDances = $prospects->getPDances($_GET['prospect']);
    }

    return $twig->render('admin/prospectInfo.html.twig', array('dances' => $dances, 'prospect' => $prospect,
        'prospectDances' => $prospectDances,
        'successes' => $GLOBALS['successes'], 'warnings' => $GLOBALS['warnings'], 'errors' => $GLOBALS['errors']));
}