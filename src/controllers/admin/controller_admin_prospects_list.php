<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 2/14/18
 * Time: 4:46 PM
 */

function admin_prospects_list($db, $twig)
{
    $prospects = new \ifds\entities\Prospects($db);
    $prospectsList = $prospects->selectAll();
    return $twig->render('admin/prospectsList.html.twig', array('prospects' => $prospectsList,
        'successes' => $GLOBALS['successes'], 'warnings' => $GLOBALS['warnings'], 'errors' => $GLOBALS['errors']));
}