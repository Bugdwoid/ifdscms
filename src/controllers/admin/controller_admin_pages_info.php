<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 1/24/18
 * Time: 4:19 PM
 */

function admin_pages_info($db, $twig)
{
    $pages = new \ifds\entities\Pages($db);
    $sections = new \ifds\entities\Sections($db);
    $columns = new \ifds\entities\Columns($db);
    $blocks = new \ifds\entities\Blocks($db);

    $page = $pages->selectOneLinkName($_GET['pagelink']);

    if (isset($_POST['pageEditBt'])) {
        if (isset($_POST['paramHugeHeader'])) {
            if ($_POST['paramHugeHeader'] === 'on') {
                $paramHugeHeader = 1;
            } else {
                $paramHugeHeader = 0;
            }
        } else {
            $paramHugeHeader = 0;
        }
        if ($pages->updateOne($_POST['name'], $_POST['pageLinkName'], $paramHugeHeader, $_POST['headerTitleText'],
                $_POST['headerCaptionText'], $_POST['headerButtonText'], $page['pageId']) === 1) {
            $GLOBALS['successes'][] = 'Modification de la page réussie !';
        } else {
            $GLOBALS['errors'][] = 'Erreur à la modification de la page.';
        }
    }

    if (isset($_POST['addSectionBt'])) {
        if ($sections->insertOne($sections->getNextRank($page['pageId'])[0], $page['pageId']) === 1) {
            $GLOBALS['successes'][] = 'Ajout de la section réussi !';
        } else {
            $GLOBALS['errors'][] = 'Erreur à l\'ajout de la section.';
        }
    }

    if (isset($_POST['addColumnBt'])) {
        if ($columns->insertOne('four', $columns->getNextRank($_POST['sectionId'])[0], $_POST['sectionId']) === 1) {
            $GLOBALS['successes'][] = 'Ajout de la colonne réussi !';
        } else {
            $GLOBALS['errors'][] = 'Erreur à l\'ajout de la colonne.';
        }
    }

    if (isset($_POST['addBlockBt'])) {
        if ($blocks->insertOne($blocks->getNextRank($_POST['columnId'])[0], $_POST['columnId']) === 1) {
            $GLOBALS['successes'][] = 'Ajout du bloc réussi !';
        } else {
            $GLOBALS['errors'][] = 'Erreur à l\'ajout du bloc.';
        }
    }

    if (isset($_POST['removeSectionBt'])) {
        if ($sections->deleteOne($_POST['sectionId']) === 1) {
            $GLOBALS['successes'][] = 'Suppression de la section réussie !';
        } else {
            $GLOBALS['errors'][] = 'Erreur à la suppression de la section.';
        }
    }

    if (isset($_POST['removeColumnBt'])) {
        if ($columns->deleteOne($_POST['columnId']) === 1) {
            $GLOBALS['successes'][] = 'Suppression de la colonne réussie !';
        } else {
            $GLOBALS['errors'][] = 'Erreur à la suppression de la colonne.';
        }
    }

    if (isset($_POST['removeBlockBt'])) {
        if ($blocks->deleteOne($_POST['blockId']) === 1) {
            $GLOBALS['successes'][] = 'Suppression du bloc réussie !';
        } else {
            $GLOBALS['errors'][] = 'Erreur à la suppression du bloc.';
        }
    }

    $page = $pages->selectOneLinkName($_GET['pagelink']);
    $pageSections = $sections->selectPage($page['pageId']);
    $pageColumns = $columns->selectFromPage($page['pageId']);
    $pageBlocks = $blocks->selectAllFromPage($page['pageId']);

    return $twig->render('admin/pagesInfo.html.twig', array('page' => $page, 'sections' => $pageSections,
        'columns' => $pageColumns, 'blocks' => $pageBlocks,
        'successes' => $GLOBALS['successes'], 'warnings' => $GLOBALS['warnings'], 'errors' => $GLOBALS['errors']));
}