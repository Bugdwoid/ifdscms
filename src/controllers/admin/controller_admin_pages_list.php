<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 1/24/18
 * Time: 2:38 PM
 */

function admin_pages_list($db, $twig)
{
    $pages = new \ifds\entities\Pages($db);

    if (isset($_POST['pageName'])) {
        if ($pages->insertOne($_POST['pageName'], $_POST['pageLinkName']) === 1) {
            $GLOBALS['successes'][] = 'Ajout de page réussi !';
        } else {
            $GLOBALS['errors'][] = 'Erreur à l\'ajout de la page.';
        }
    }

    if (isset($_POST['deletePageBt'])) {
        if ($pages->deleteOne($_POST['deletePageId']) === 1) {
            $GLOBALS['successes'][] = 'Suppression de la page réussie !';
        } else {
            $GLOBALS['errors'][] = 'Erreur à la suppression de la page.';
        }
    }

    $pagesArray = $pages->selectAll();

    return $twig->render('admin/pagesList.html.twig', array('pages' => $pagesArray,
        'successes' => $GLOBALS['successes'], 'warnings' => $GLOBALS['warnings'], 'errors' => $GLOBALS['errors']));
}