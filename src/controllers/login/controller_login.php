<?php
/**
 * Created by IntelliJ IDEA.
 * User: eleve
 * Date: 05/01/18
 * Time: 23:13
 */

function login($db, $twig)
{
    $userArray = "Pas d'utilisateur";
    if (isset($_SESSION['userid'], $_SESSION['email'], $_SESSION['role'])) {
        header('Location: index.php?page=admin_home');
    } else {
        $postArray = var_export($_POST, true);
        if (isset($_POST['email'], $_POST['password'])) {
            if (!empty($_POST['email'])) {
                if (!empty($_POST['password'])) {
                    $users = new ifds\entities\Users($db);
                    $user = $users->loginUser($_POST['email']);
                    $userArray = var_export($user, true);
                    if (!empty($user)) {
                        if (password_verify($_POST['password'], $user['password'])) {
                            $_SESSION['userid'] = $user['id'];
                            $_SESSION['email'] = $user['email'];
                            $_SESSION['role'] = 1;
                            header('Location: index.php?page=admin_home');
                        } else {
                            $GLOBALS['warnings'][] = 'Le mot de passe est incorrect.';
                        }
                    } else {
                        $GLOBALS['warnings'][] = 'L\'utilisateur n\'existe pas.';
                    }
                } else {
                    $GLOBALS['warnings'][] = 'Aucun mot de passe n\'a été entré.';
                }
            } else {
                $GLOBALS['warnings'][] = 'Aucun nom d\'utilisateur n\'a été entré.';
            }
        }

        /*echo $text = password_hash("", PASSWORD_BCRYPT);*/

        $warningsArray = var_export($GLOBALS['warnings'], true);
        $sessionArray = var_export($_SESSION, true);

        return $twig->render('login/login.html.twig', array('warningsArray' => $warningsArray, 'session' => $sessionArray,
            'user' => $userArray, 'post' => $postArray,
            'successes' => $GLOBALS['successes'], 'warnings' => $GLOBALS['warnings'], 'errors' => $GLOBALS['errors']));
    }
}