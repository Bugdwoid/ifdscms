<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 1/25/18
 * Time: 4:16 PM
 */

use JeroenDesloovere\VCard\VCard;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Displays user-created pages
 *
 * @param PDO $db
 * @param Twig_Environment $twig
 * @param null $optionalPage
 * @return mixed
 * @throws Twig_Error_Loader
 * @throws Twig_Error_Runtime
 * @throws Twig_Error_Syntax
 * @throws \PHPMailer\PHPMailer\Exception
 */
function custompage($db, $twig, $optionalPage = null)
{
    $blocks = new \ifds\entities\Blocks($db);
    $columns = new \ifds\entities\Columns($db);
    $dances = new \ifds\entities\Dances($db);
    $pages = new \ifds\entities\Pages($db);
    $planning = new \ifds\entities\Planning($db);
    $sections = new \ifds\entities\Sections($db);
    $topmenu = new \entities\Topmenu($db);

    $danceTypes = $dances->selectAll();

    if (isset($_POST['quotationFormBt'])) {
        var_dump($_POST);

        //<editor-fold desc="Form validation">
        $validation = true;

        $lastname = $_POST['lastname'];
        if (strlen($lastname) > 255) {
            $validation = false;
            echo 'Erreur au nom';
        }
        if (strlen($lastname) === 0) {
            $validation = false;
            echo 'Entrez votre nom';
        }

        $firstname = $_POST['firstname'];
        if (strlen($firstname) > 255) {
            $validation = false;
            echo 'Erreur au prénom';
        }
        if (strlen($firstname) === 0) {
            $validation = false;
            echo 'Entrez votre prénom';
        }

        $birthday = $_POST['birthday'];
        $dateTime = DateTime::createFromFormat('d-m-Y', $birthday);
        $errors = DateTime::getLastErrors();
        if (DateTime::createFromFormat('d-m-Y', $birthday) === FALSE || !empty($errors['warning_count'])) {
            // Tries to format the date to see if it's valid, otherwise if it is, unvalidate the form
            // Then looks at the DateTime warnings and unvalidates the form if there are warnings (like for 38/04/1984 -> not valid)
            $validation = false;
            echo 'Erreur à la date de naissance';
        }

        $address = $_POST['address'];
        if (strlen($address) > 255) {
            $validation = false;
            echo 'Erreur à l\'adresse';
        }
        if (strlen($address) < 0) {
            $validation = false;
            echo 'Entrez votre adresse';
        }

        $postalcode = $_POST['postalcode'];
        if (strlen($postalcode) > 16) {
            $validation = false;
            echo 'Erreur au code postal';
        }
        if (strlen($postalcode) < 0) {
            $validation = false;
            echo 'Erreur au code postal';
        }

        $city = $_POST['city'];
        if (strlen($city) > 255) {
            $validation = false;
            echo 'Erreur à la ville';
        }

        $phone1 = $_POST['phone1'];
        if (strlen($phone1) > 20) {
            $validation = false;
            echo 'Erreur au tel1';
        }

        $phone2 = '';
        if (isset($_POST['phone2'])) {
            $phone2 = $_POST['phone2'];
            if (strlen($phone2) > 20) {
                $validation = false;
                echo 'Erreur au tel2';
            }
        }

        $email = $_POST['email'];
        if (strlen($email) > 255 || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $validation = false;
            echo 'Erreur à l\'email';
        }

        $website = '';
        if (isset($_POST['website']) && !empty($_POST['website'])) {
            $website = $_POST['website'];
            if (strlen($website) > 255) {
                $validation = false;
                echo 'Erreur au site web';
            }
        }

        $dancer = 0;
        $dancesDancer = array();
        $teacher = 0;
        $dancesTeacher = array();
        $competitor = 0;
        $dancesCompetitor = array();

        if ((isset($_POST['dancer']) && !empty(isset($_POST['dancer'])))
            || (isset($_POST['teacher']) && !empty($_POST['teacher']))
            || (isset($_POST['competitor']) && !empty($_POST['competitor']))) {

            if (isset($_POST['dancer']) && !empty(isset($_POST['dancer']))) {
                if ($_POST['dancer'] === 'on') {
                    $dancer = 1;
                } else {
                    $dancer = 0;
                }
                if ($dancer === 1) {
                    if (isset($_POST['dancesDancer'])) {
                        $dancesDancer = $_POST['dancesDancer'];
                    }
                } else {
                    $validation = false;
                    echo 'Erreur à sélection danse';
                }
            }

            if (isset($_POST['teacher']) && !empty($_POST['teacher'])) {
                if ($_POST['teacher'] === 'on') {
                    $teacher = 1;
                } else {
                    $teacher = 0;
                }
                if ($teacher === 1) {
                    if (isset($_POST['dancesTeacher'])) {
                        $dancesTeacher = $_POST['dancesTeacher'];
                    }
                } else {
                    $validation = false;
                    echo 'Erreur à sélection prof';
                }
            }

            if (isset($_POST['competitor']) && !empty($_POST['competitor'])) {
                if ($_POST['competitor'] === 'on') {
                    $competitor = 1;
                } else {
                    $competitor = 0;
                }
                if ($competitor === 1) {
                    if (isset($_POST['dancesCompetitor'])) {
                        $dancesCompetitor = $_POST['dancesCompetitor'];
                    }
                } else {
                    $validation = false;
                    echo 'Erreur à sélection compétiteur';
                }
            }
        } else {
            $validation = false;
            echo 'Vous devez au moins choisir un des choix entre Danseu·r·se, Enseignant·e ou Compétit·eur·rice';
        }

        $funding = $_POST['funding'];
        if (strlen($funding) > 255) {
            $validation = false;
            echo 'Erreur à sélection financement';
        }

        $sendquotation = 0;
        $sendfinancingfile = 0;
        $callme = 0;
        $callmeNumber = '';
        $callmeDatetime = '';
        if ((isset($_POST['sendquotation']) && !empty($_POST['sendquotation']))
            || (isset($_POST['sendfinancingfile']) && !empty($_POST['sendfinancingfile']))
            || (isset($_POST['callme']) && !empty($_POST['callme']))) {
            if (isset($_POST['sendquotation']) && !empty($_POST['sendquotation'])) {
                if ($_POST['sendquotation'] === 'on') {
                    $sendquotation = 1;
                } else {
                    $sendquotation = 0;
                }
                if ($sendquotation !== 1) {
                    $validation = false;
                    echo 'Erreur à sélection devis';
                }
            }


            if (isset($_POST['sendfinancingfile']) && !empty($_POST['sendfinancingfile'])) {
                if ($_POST['sendfinancingfile'] === 'on') {
                    $sendfinancingfile = 1;
                } else {
                    $sendfinancingfile = 0;
                }
                if ($sendfinancingfile !== 1) {
                    $validation = false;
                    echo 'Erreur à sélection demande financement';
                }
            }


            if (isset($_POST['callme']) && !empty($_POST['callme'])) {
                if ($_POST['callme'] === 'on') {
                    $callme = 1;

                    if (isset($_POST['callmeNumber']) && !empty($_POST['callmeNumber'])) {
                        $callmeNumber = $_POST['callmeNumber'];
                        if (strlen($phone1) > 20) {
                            $validation = false;
                            echo 'Erreur à num appelez moi';
                        }
                    }

                    if (isset($_POST['callmeDate']) && !empty($_POST['callmeDate'])) {
                        $callmeDatetime = $_POST['callmeDate'];
                        $dateTime = DateTime::createFromFormat('d-m-Y H:i:s', $callmeDatetime);
                        $errors = DateTime::getLastErrors();
                        if (!empty($errors['warning_count'])) {
                            // Tries to format the date to see if it's valid, otherwise if it is, unvalidate the form
                            // Then looks at the DateTime warnings and unvalidates the form if there are warnings (like for 38/04/1984 -> not valid)
                            $validation = false;
                            echo 'Erreur à date appelez moi';
                        }
                    }
                } else {
                    $callme = 0;
                }
                if ($callme !== 1) {
                    $validation = false;
                    echo 'Erreur à sélection appelez-moi';
                }
            }
        } else {
            $validation = false;
            echo 'Vous devez au moins choisir un des éléments dans "envoyez-moi"';
        }

        $message = '';
        if (isset($_POST['message']) && !empty($_POST['message'])) {
            $message = $_POST['message'];
        }

        //</editor-fold>

        if (!$validation) {
            echo '<br><br>Erreur dans l\'insertion des données';
        } else {
            echo '<br><br>Données correctes, traitement des données en cours';
            $dateBDay = strtotime($birthday);
            $dateTimeCallme = strtotime($callmeDatetime);

            //<editor-fold desc="Data insert into the database">
            $prospects = new ifds\entities\Prospects($db);

            $prospectId = '';
            if ($prospects->insertOne(date("Y/m/d H:i:s"), $lastname, $firstname, date("Y/m/d", $dateBDay),
                    $address, $postalcode, $city, $phone1, $phone2, $email, $website, $dancer, $teacher, $competitor,
                    $funding, $sendquotation, $sendfinancingfile, $callme, $callmeNumber,
                    date("Y/m/d H:i:s", $dateTimeCallme), $message) === 1) {
                echo 'db ok';
                $prospectId = $db->lastInsertId();
                foreach ($dancesDancer as $dance) {
                    if ($prospects->insertPDance($prospectId, 'dancer', $dance) === 1) {
                        echo $dance . 'inserted in dancer registry';
                    }
                }
                foreach ($dancesTeacher as $dance) {
                    if ($prospects->insertPDance($prospectId, 'teacher', $dance) === 1) {
                        echo $dance . 'inserted in teacher registry';
                    }
                }
                foreach ($dancesCompetitor as $dance) {
                    if ($prospects->insertPDance($prospectId, 'competitor', $dance) === 1) {
                        echo $dance . 'inserted in competitor registry';
                    }
                }
            } else {
                echo 'db pas ok';
            }
            //</editor-fold>

            //<editor-fold desc="Vcard generation">
            $vcard = new VCard();
            $vcard->addName($lastname, $firstname);
            $vcard->addBirthday(date("Y-m-d", $dateBDay));
            $vcard->addAddress(null, null, $address, $city, null, $postalcode, 'France', 'HOME');
            $vcard->addPhoneNumber($phone1, "PREF;HOME");
            if (isset($phone2)) {
                $vcard->addPhoneNumber($phone2, "HOME");
            }
            $vcard->addEmail($email);
            if (isset($website)) {
                $vcard->addURL($website);
            }
            $vcard->addNote($message);
            try {
                $vcard->setSavePath(getcwd() . '/userdata/vcard/');
            } catch (\JeroenDesloovere\VCard\VCardException $e) {
                return highlight_string("<?php\n\$data =\n" . print_r($e, true) . ";\n?>");
            }
            $vcard->setFilename($prospectId . '-' . strtolower($firstname) . '-' . strtolower($lastname));
            $vcard->save();
            //</editor-fold>

            //<editor-fold desc="Mail sending">
            $thisProspect = $prospects->selectOne($prospectId);
            $prospectDances = $prospects->getPDances($prospectId);

            $template = $twig->loadTemplate('mail/quotationMail.html.twig');
            $subject = $template->renderBlock('subject', array('prospect' => $thisProspect));
            $htmlResult = $template->renderBlock('body_html', array('dances' => $danceTypes, 'prospect' => $thisProspect,
                'prospectDances' => $prospectDances));
            $textResult = $template->renderBlock('body_text', array('dances' => $danceTypes, 'prospect' => $thisProspect,
                'prospectDances' => $prospectDances));

            //Create a new PHPMailer instance
            $mail = new PHPMailer(true);
            //Set charset as utf8
            $mail->CharSet = 'UTF-8';
            // Set PHPMailer to use the sendmail transport
            $mail->isSendmail();
            //Set who the message is to be sent from
            $mail->setFrom($GLOBALS['settings']['mailSenderAddress'], $GLOBALS['settings']['mailSender']);
            //Set who the message is to be sent to
            $mail->addAddress($GLOBALS['settings']['mailRecipientAddress'], $GLOBALS['settings']['mailRecipient']);
            //Set the subject line
            $mail->Subject = $subject;
            //Set the content
            $mail->Body = $htmlResult;
            //Set the text version
            $mail->AltBody = $textResult;
            //Add vCard as attachment
            $mail->addAttachment(getcwd() . '/userdata/vcard/' . $thisProspect['prospectId'] . '-' .
                strtolower($thisProspect['firstname']) . '-' . strtolower($thisProspect['lastname']) . '.vcf');
            //send the message, check for errors
            if (!$mail->send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
            } else {
                echo "Message sent!";
            }
            //</editor-fold>
        }
    }

    $listePages = $pages->selectAll();

    if ($optionalPage !== null) {
        $page = $pages->selectOneLinkName($optionalPage);
    } else if (isset($_GET['page'])) {
        $page = $pages->selectOneLinkName($_GET['page']);
    } else {
        $page = $pages->selectOneLinkName($GLOBALS['settings']['homePage']);
    }
    $topmenuPrimary = $topmenu->selectPrimary();
    $topmenuChildren = $topmenu->selectChildren();
    $pageSections = $sections->selectPage($page['pageId']);
    $pageColumns = $columns->selectFromPage($page['pageId']);
    $pageBlocks = $blocks->selectAllFromPage($page['pageId']);
    $fullPlanning = $planning->selectAll();

    return $twig->render('frontend/custompage.html.twig', array('listePages' => $listePages, 'page' => $page,
        'sections' => $pageSections, 'columns' => $pageColumns, 'blocks' => $pageBlocks, 'dances' => $danceTypes,
        'planning' => $fullPlanning, 'primaryTopmenu' => $topmenuPrimary, 'secondaryTopmenu' => $topmenuChildren,
        'successes' => $GLOBALS['successes'], 'warnings' => $GLOBALS['warnings'], 'errors' => $GLOBALS['errors']));
}