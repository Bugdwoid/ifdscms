<?php

use PHPMailer\PHPMailer\PHPMailer;

/**
 * Created by IntelliJ IDEA.
 * User: eleve
 * Date: 05/01/18
 * Time: 23:04
 * @throws \PHPMailer\PHPMailer\Exception
 */

function home($db, $twig)
{
    $pages = new \ifds\entities\Pages($db);
    $listePages = $pages->selectAll();

    return $twig->render('frontend/index.html.twig', array('listePages' => $listePages,
        'successes' => $GLOBALS['successes'], 'warnings' => $GLOBALS['warnings'], 'errors' => $GLOBALS['errors']));
}