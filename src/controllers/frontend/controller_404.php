<?php
/**
 * Created by IntelliJ IDEA.
 * User: bugdwoid
 * Date: 6/8/18
 * Time: 2:54 PM
 */

function error_404($db, $twig)
{
    $pages = new \ifds\entities\Pages($db);
    $listePages = $pages->selectAll();
    return $twig->render('frontend/404.html.twig', array('listePages' => $listePages));
}