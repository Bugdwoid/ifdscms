-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 27, 2018 at 11:18 AM
-- Server version: 10.2.14-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ifdscms`
--

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

CREATE TABLE `blocks` (
  `blockId` int(11) NOT NULL,
  `blockStyle` enum('card','nocard') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blockBackground` int(11) DEFAULT NULL,
  `columnOrder` int(11) NOT NULL,
  `columnId` int(11) NOT NULL,
  `contentType` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `columns`
--

CREATE TABLE `columns` (
  `columnId` int(11) NOT NULL,
  `columnWidth` enum('one','two','three','four','five','six','seven','eight','nine','ten','eleven','twelve','thirteen','fourteen','fifteen','sixteen') COLLATE utf8mb4_unicode_ci NOT NULL,
  `sectionOrder` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contents_html`
--

CREATE TABLE `contents_html` (
  `htmlTitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `htmlContent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blockId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contents_images`
--

CREATE TABLE `contents_images` (
  `imageTitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'NULL',
  `imageSource` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paramFill` tinyint(1) NOT NULL,
  `imgSize` int(11) NOT NULL DEFAULT 5,
  `blockId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contents_textimage`
--

CREATE TABLE `contents_textimage` (
  `textimageTitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `textimageImgSource` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `textimageTextContent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `textimageImgPos` int(11) NOT NULL DEFAULT 1,
  `imgSize` int(11) NOT NULL DEFAULT 5,
  `blockId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contentTypes`
--

CREATE TABLE `contentTypes` (
  `typeId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `danceTypes`
--

CREATE TABLE `danceTypes` (
  `danceId` int(11) NOT NULL,
  `danceName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `imgSize`
--

CREATE TABLE `imgSize` (
  `sizeId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `pageId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pageLinkName` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paramHugeHeader` tinyint(1) NOT NULL DEFAULT 0,
  `headerTitleText` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `headerCaptionText` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `headerButtonText` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pagesSlideshows`
--

CREATE TABLE `pagesSlideshows` (
  `bgId` int(11) NOT NULL,
  `pageId` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prevTransition` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nextTransition` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `planning`
--

CREATE TABLE `planning` (
  `pDateId` int(11) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prospectDances`
--

CREATE TABLE `prospectDances` (
  `pdanceId` int(11) NOT NULL,
  `prospectId` int(11) NOT NULL,
  `danceAs` enum('dancer','teacher','competitor') COLLATE utf8mb4_unicode_ci NOT NULL,
  `danceType` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prospects`
--

CREATE TABLE `prospects` (
  `prospectId` int(11) NOT NULL,
  `dateInsert` datetime NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postalcode` int(16) NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone1` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dancer` tinyint(1) NOT NULL,
  `teacher` tinyint(1) NOT NULL,
  `competitor` tinyint(1) NOT NULL,
  `funding` enum('myself','association','financer','dontknow') COLLATE utf8mb4_unicode_ci NOT NULL,
  `sendquotation` tinyint(1) NOT NULL,
  `sendfinancingfile` tinyint(1) NOT NULL,
  `callme` tinyint(1) NOT NULL,
  `callme_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `callme_datetime` datetime DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `sectionId` int(11) NOT NULL,
  `headerContent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pageOrder` int(11) NOT NULL,
  `pageId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` enum('1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `homePage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mailSender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mailSenderAddress` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mailRecipient` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mailRecipientAddress` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `textimage_imgposition`
--

CREATE TABLE `textimage_imgposition` (
  `posId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `topmenu`
--

CREATE TABLE `topmenu` (
  `linkId` int(11) NOT NULL,
  `text` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menuOrder` int(11) NOT NULL,
  `isPrimary` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `pageId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`blockId`),
  ADD KEY `columnId` (`columnId`),
  ADD KEY `contentType` (`contentType`);

--
-- Indexes for table `columns`
--
ALTER TABLE `columns`
  ADD PRIMARY KEY (`columnId`),
  ADD KEY `pagePartId` (`sectionId`);

--
-- Indexes for table `contents_html`
--
ALTER TABLE `contents_html`
  ADD PRIMARY KEY (`blockId`),
  ADD UNIQUE KEY `blockId` (`blockId`) USING BTREE;

--
-- Indexes for table `contents_images`
--
ALTER TABLE `contents_images`
  ADD PRIMARY KEY (`blockId`),
  ADD UNIQUE KEY `blockId` (`blockId`) USING BTREE,
  ADD KEY `imgSize` (`imgSize`);

--
-- Indexes for table `contents_textimage`
--
ALTER TABLE `contents_textimage`
  ADD PRIMARY KEY (`blockId`),
  ADD UNIQUE KEY `blockId` (`blockId`) USING BTREE,
  ADD KEY `textimageImgPos` (`textimageImgPos`),
  ADD KEY `imgSize` (`imgSize`);

--
-- Indexes for table `contentTypes`
--
ALTER TABLE `contentTypes`
  ADD PRIMARY KEY (`typeId`);

--
-- Indexes for table `danceTypes`
--
ALTER TABLE `danceTypes`
  ADD PRIMARY KEY (`danceId`);

--
-- Indexes for table `imgSize`
--
ALTER TABLE `imgSize`
  ADD PRIMARY KEY (`sizeId`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`pageId`),
  ADD UNIQUE KEY `pageLinkName` (`pageLinkName`);

--
-- Indexes for table `pagesSlideshows`
--
ALTER TABLE `pagesSlideshows`
  ADD PRIMARY KEY (`bgId`),
  ADD KEY `pageId` (`pageId`);

--
-- Indexes for table `planning`
--
ALTER TABLE `planning`
  ADD PRIMARY KEY (`pDateId`);

--
-- Indexes for table `prospectDances`
--
ALTER TABLE `prospectDances`
  ADD PRIMARY KEY (`pdanceId`),
  ADD KEY `prospectDances_ibfk_2` (`prospectId`),
  ADD KEY `danceType` (`danceType`);

--
-- Indexes for table `prospects`
--
ALTER TABLE `prospects`
  ADD PRIMARY KEY (`prospectId`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`sectionId`),
  ADD KEY `pageId` (`pageId`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `homePage` (`homePage`);

--
-- Indexes for table `textimage_imgposition`
--
ALTER TABLE `textimage_imgposition`
  ADD PRIMARY KEY (`posId`);

--
-- Indexes for table `topmenu`
--
ALTER TABLE `topmenu`
  ADD PRIMARY KEY (`linkId`),
  ADD KEY `parent` (`parent`),
  ADD KEY `pageId` (`pageId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blocks`
--
ALTER TABLE `blocks`
  MODIFY `blockId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `columns`
--
ALTER TABLE `columns`
  MODIFY `columnId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `contentTypes`
--
ALTER TABLE `contentTypes`
  MODIFY `typeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `danceTypes`
--
ALTER TABLE `danceTypes`
  MODIFY `danceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `imgSize`
--
ALTER TABLE `imgSize`
  MODIFY `sizeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `pageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pagesSlideshows`
--
ALTER TABLE `pagesSlideshows`
  MODIFY `bgId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `planning`
--
ALTER TABLE `planning`
  MODIFY `pDateId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `prospectDances`
--
ALTER TABLE `prospectDances`
  MODIFY `pdanceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `prospects`
--
ALTER TABLE `prospects`
  MODIFY `prospectId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `sectionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `textimage_imgposition`
--
ALTER TABLE `textimage_imgposition`
  MODIFY `posId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `topmenu`
--
ALTER TABLE `topmenu`
  MODIFY `linkId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blocks`
--
ALTER TABLE `blocks`
  ADD CONSTRAINT `blocks_ibfk_1` FOREIGN KEY (`columnId`) REFERENCES `columns` (`columnId`),
  ADD CONSTRAINT `blocks_ibfk_2` FOREIGN KEY (`contentType`) REFERENCES `contentTypes` (`typeId`);

--
-- Constraints for table `columns`
--
ALTER TABLE `columns`
  ADD CONSTRAINT `columns_ibfk_1` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`sectionId`);

--
-- Constraints for table `contents_html`
--
ALTER TABLE `contents_html`
  ADD CONSTRAINT `contents_html_ibfk_1` FOREIGN KEY (`blockId`) REFERENCES `blocks` (`blockId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contents_images`
--
ALTER TABLE `contents_images`
  ADD CONSTRAINT `contents_images_ibfk_1` FOREIGN KEY (`blockId`) REFERENCES `blocks` (`blockId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contents_images_ibfk_2` FOREIGN KEY (`imgSize`) REFERENCES `imgSize` (`sizeId`);

--
-- Constraints for table `contents_textimage`
--
ALTER TABLE `contents_textimage`
  ADD CONSTRAINT `contents_textimage_ibfk_1` FOREIGN KEY (`blockId`) REFERENCES `blocks` (`blockId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contents_textimage_ibfk_2` FOREIGN KEY (`textimageImgPos`) REFERENCES `textimage_imgposition` (`posId`),
  ADD CONSTRAINT `contents_textimage_ibfk_3` FOREIGN KEY (`imgSize`) REFERENCES `imgSize` (`sizeId`);

--
-- Constraints for table `pagesSlideshows`
--
ALTER TABLE `pagesSlideshows`
  ADD CONSTRAINT `pagesSlideshows_ibfk_1` FOREIGN KEY (`pageId`) REFERENCES `pages` (`pageId`);

--
-- Constraints for table `prospectDances`
--
ALTER TABLE `prospectDances`
  ADD CONSTRAINT `prospectDances_ibfk_2` FOREIGN KEY (`prospectId`) REFERENCES `prospects` (`prospectId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prospectDances_ibfk_3` FOREIGN KEY (`danceType`) REFERENCES `danceTypes` (`danceId`);

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_ibfk_1` FOREIGN KEY (`pageId`) REFERENCES `pages` (`pageId`);

--
-- Constraints for table `settings`
--
ALTER TABLE `settings`
  ADD CONSTRAINT `settings_ibfk_1` FOREIGN KEY (`homePage`) REFERENCES `pages` (`pageLinkName`);

--
-- Constraints for table `topmenu`
--
ALTER TABLE `topmenu`
  ADD CONSTRAINT `topmenu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `topmenu` (`linkId`),
  ADD CONSTRAINT `topmenu_ibfk_2` FOREIGN KEY (`pageId`) REFERENCES `pages` (`pageId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
