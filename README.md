# IFDSCMS

This is a WIP website/page manager written in PHP using Twig for the layouts.

## Prerequisites

This has been developed with PHP 7.1 and MariaDB 10.2.

It needs at least PHP 7.0 to work, and the database structure currently requires
MariaDB 10.2 to import correctly (it should work on MySQL 5.7 but hasn't been tested).

## Installation

To run this, you need to download its dependencies with composer :

`composer update`



You'll also need to set-up your MySQL connection in `src/dbconnect.php` :

```
$config['server'] = '';
$config['login'] = '';
$config['password'] = '';
$config['db'] = '';
```

The database structure file is "ifdscms.sql", at this project's root.

Then you'll need to create an admin account, in the 'users' table, with an email address, 
a role value of "1", and a hashed password generated with this line of PHP 
(you just need to put your password between the quotation marks) :

```echo $text = password_hash("", PASSWORD_BCRYPT);```

The home page won't exist so it will show an error, but you should then be able to go to 
`[project address]/index.php?page=login` to connect to the admin interface.

From there, you should be able to create a page and set it as the home page in the Settings 
("Paramètres" as of now, as the interface's strings are not yet translatable).

You can then go to the home page and it should show up.

You can't edit the menu bar from the admin interface yet, but it's planned and will come soon.