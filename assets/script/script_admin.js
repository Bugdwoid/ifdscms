$(document).ready(function () {
    $('.ui.dropdown').dropdown();
    $('.ui.styled.accordion').accordion();
    $('.message .close')
        .on('click', function () {
            $(this)
                .closest('.message')
                .transition('fade')
            ;
        })
    ;

    $('#startDatepicker').calendar({
        ampm: false,
        firstDayOfWeek: 1,
        type: 'date',
        formatter: {
            date: function (date) {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return day + '-' + month + '-' + year;
            }
        },
        monthFirst: false,
        startMode: 'year',
        text: {
            days: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthsShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juill.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
            today: 'Aujourd\'hui',
            now: 'Maintenant'
        }
    });

    $('#endDatepicker').calendar({
        ampm: false,
        firstDayOfWeek: 1,
        type: 'date',
        formatter: {
            date: function (date) {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return day + '-' + month + '-' + year;
            }
        },
        monthFirst: false,
        startMode: 'year',
        text: {
            days: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthsShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juill.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
            today: 'Aujourd\'hui',
            now: 'Maintenant'
        }
    });

    $('.ui.checkbox')
        .checkbox()
    ;

    var check = $('.hugeHeaderCb').checkbox('is checked');
    if (check) {
        $("#hugeHeaderParams").show();
    } else {
        $("#hugeHeaderParams").hide();
    }

    $('.iframe-button').fancybox({
        'type': 'iframe'
    });
});

$('.hugeHeaderCb').click(function () {
    var check = $('.hugeHeaderCb').checkbox('is unchecked');
    if (check) {
        $("#hugeHeaderParams").show("100");
    } else {
        $("#hugeHeaderParams").hide("100");
    }
});

function responsive_filemanager_callback(field_id){
    var field = jQuery('#'+field_id);
    field.val('assets/uploaded/' + field.val());
}