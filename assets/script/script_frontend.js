$(document).ready(function () {
    // fix menu when passed
    $('.masthead')
        .visibility({
            once: false,
            onBottomPassed: function () {
                $('.fixed.menu').transition('fade in');
            },
            onBottomPassedReverse: function () {
                $('.fixed.menu').transition('fade out');
            }
        });
    // create sidebar and attach to menu open
    $('.ui.sidebar')
        .sidebar('attach events', '.toc.item')
    ;

    var slickElement = $('.js-slick');
    slickElement.slick({
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
        draggable: false,
        fade: true,
        speed: 1000
    });

    slickElement.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        $(slick.$slides).removeClass('is-animating');
    });

    slickElement.on('afterChange', function (event, slick, currentSlide, nextSlide) {
        $(slick.$slides.get(currentSlide)).addClass('is-animating');
    });

    var today = new Date();
    $('#bdayDatepicker').calendar({
        ampm: false,
        firstDayOfWeek: 1,
        type: 'date',
        formatter: {
            date: function (date) {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return day + '-' + month + '-' + year;
            }
        },
        monthFirst: false,
        maxDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
        startMode: 'year',
        text: {
            days: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthsShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juill.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
            today: 'Aujourd\'hui',
            now: 'Maintenant'
        }
    });

    $('#callDatepicker').calendar({
        ampm: false,
        firstDayOfWeek: 1,
        formatter: {
            date: function (date) {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                var hours = date.getHours();
                return day + '-' + month + '-' + year;
            }
        },
        monthFirst: false,
        minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
        startMode: 'year',
        text: {
            days: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
            months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthsShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juill.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
            today: 'Aujourd\'hui',
            now: 'Maintenant'
        }
    });

    $('.ui.radio.checkbox')
        .checkbox()
    ;

    $('.ui.checkbox')
        .checkbox()
    ;

    $('.ui.dropdown')
        .dropdown()
    ;

    $(".dancesField").hide();
    $("#calldatetimeFields").hide();

    window.sr = ScrollReveal();
    sr.reveal('.block');
});

$('.dancerCb').click(function () {
    var check = $('.dancerCb').checkbox('is unchecked');
    console.log(check);
    if (check) {
        $("#dancesDancerDd").show("100");
    } else {
        $("#dancesDancerDd").hide("100");
    }
});

$('.teacherCb').click(function () {
    if ($('.teacherCb').checkbox('is unchecked')) {
        $("#dancesTeacherDd").show("100");
    } else {
        $("#dancesTeacherDd").hide("100");
    }
});

$('.competitorCb').click(function () {
    var check = $('.competitorCb').checkbox('is unchecked');
    console.log(check);
    if (check) {
        $("#dancesCompetitorDd").show("100");
    } else {
        $("#dancesCompetitorDd").hide("100");
    }
});

$('.callmeatCb').click(function () {
    var check = $('.callmeatCb').checkbox('is unchecked');
    console.log(check);
    if (check) {
        $("#calldatetimeFields").show("100");
    } else {
        $("#calldatetimeFields").hide("100");
    }
});